""" peepo bot """
# pylint: disable=arguments-differ,unbalanced-tuple-unpacking

import logging
import os
import random
import subprocess
import traceback

from configparser import ConfigParser
from shutil import which

import discord
import praw

from discord.ext import commands
from discord.ext.commands import CommandNotFound


def run_command(cmd_list):
    """ Executes commands and returns the output from stdout """
    with open(os.devnull, "w") as stderr:
        stdout = subprocess.PIPE
        process = subprocess.run(cmd_list, stdout=stdout, stderr=stderr,
                                 check=False)
        return process.stdout.decode("utf-8")


class Peepo(commands.Bot):
    """
    The main bot
    """
    def __init__(self, config):
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(
            command_prefix=config.get("setup", "prefix"),
            intents=intents
        )

        self.config = config

    async def on_ready(self):
        """ Setup after initialisation """
        logging.log(logging.INFO, "Successfully logged in")

        status = discord.Status.idle
        activity = discord.Game(name=self.config.get("status", "active"))
        await self.change_presence(status=status, activity=activity)

    async def on_message(self, message):
        """ Executes on every message sent """
        if message.author.bot:
            return
        await self.process_commands(message)

    async def on_error(self, event_method, *args, **kwargs):
        """ Handles errors """

    async def on_command_error(self, ctx, e):
        """ Handles command errors """
        if isinstance(e, CommandNotFound):
            return
        e_traceback = e.__traceback__
        e_str = "".join(traceback.format_exception(type(e), e, e_traceback))
        logging.log(logging.ERROR, "Exception caught:\n%s", e_str)
        await ctx.send(e)


class General(commands.Cog):
    """
    General commands category
    """
    def __init__(self, bot, reddit_client):
        self.bot = bot
        self.reddit_client = reddit_client

    @commands.command()
    async def say(self, ctx, *args):
        """ Echoes messages back """
        msg = ["\"{}\"".format(i) if " " in i else i for i in args]
        await ctx.send(" ".join(msg))

    @commands.command()
    async def roll(self, ctx, *args):
        """ Rolls a random number """
        try:
            author = ctx.message.author
            low = 0 if len(args) <= 1 else int(args[0])
            high = 100 if len(args) == 0 else int(args[-1])
            rand_num = random.randint(low, high)

            await ctx.send("{} has rolled a {}".format(author, rand_num))
        except ValueError:
            await ctx.send("[roll] Invalid arguments: {}".format(args))

    @commands.command()
    async def repo(self, ctx):
        """ Returns the main repository url """
        await ctx.send("https://www.gitlab.com/julian-heng/peepo")

    @commands.command()
    async def sort(self, ctx, *args):
        """ Sorts the given message """
        if len(args) == 0:
            await ctx.send("_ _")
            return

        args = list(args)
        args.sort()
        await ctx.send("\n".join(args))

    @commands.command()
    async def crash(self, ctx, *args):
        """ Raise an exception """
        raise Exception("Crashed!!!")

    @commands.command()
    async def fortune(self, ctx):
        """ Display a random fortune """
        if which("fortune") is None:
            raise Exception("Missing fortune program")

        await ctx.send("Peepo says:\n>>> {}".format(run_command(["fortune"])))

    @commands.command()
    async def sponge(self, ctx, *args):
        """ "Spongifies" a given message """
        def spongecase(_str):
            def rand_case(i):
                return i.swapcase() if random.choice([True, False]) else i
            return "".join(map(rand_case, _str))

        if len(args) == 0:
            await ctx.send("_ _")
            return

        out = " ".join(spongecase(i) for i in args)

        await ctx.send("{}".format(out))

    @commands.command()
    async def reddit(self, ctx, *args):
        """ Displays reddit posts depending on arguments """
        err_fmt = "{{}}: {}reddit [subreddit] [hot|top] {{{{limit}}}}"
        err_fmt = err_fmt.format(self.bot.command_prefix)

        if len(args) == 0 or len(args) == 1:
            await ctx.send(err_fmt.format("Not enough arguments"))
            return

        sub, mode, limit = (list(args) + ([None] * 1))[:3]

        sub = self.reddit_client.subreddit(sub)
        limit = 3 if limit is None else int(limit)

        modes = {
            "hot": sub.hot,
            "top": sub.top
        }

        if mode not in modes:
            await ctx.send(err_fmt.format("Invalid mode"))
            return

        fmt = "{}. {}"
        posts = modes[mode](limit=limit)
        out = "\n".join(fmt.format(n + 1, i) for n, i in enumerate(posts))
        await ctx.send(out)

    @commands.command()
    async def flip(self, ctx):
        """ Flip a coin """
        fmt = "You flipped {}!"
        await ctx.send(fmt.format(random.choice(["heads", "tails"])))

    @commands.command()
    async def ping(self, ctx):
        """ Display latency information """
        latency = round(self.bot.latency * 1000)
        colour = int(self.bot.config.get("colours", "main"), base=16)

        embed = discord.Embed(
            title="Pong!",
            description="{}ms".format(latency),
            colour=discord.Colour(colour)
        )
        await ctx.send(embed=embed)

    @commands.command()
    async def nocontext(self, ctx, user: discord.User):
        """ Selects a random message from a given user out of context """
        def filter_channels(chan):
            """ Checks if the channel is a text channel """
            return isinstance(chan, discord.TextChannel)

        def filter_msg(msg, user):
            """ Checks that the message is valid """
            prefix = self.bot.config["setup"]["prefix"]
            return (
                msg.author == user
                and msg.clean_content
                and not msg.content.startswith(prefix)
            )

        # Get all text channels
        channels = [i for i in ctx.guild.channels if filter_channels(i)]
        user_msg = []

        # TODO: Do we need to have a limit when searching through all channels?
        for channel in channels:
            try:
                async for msg in channel.history():
                    if filter_msg(msg, user):
                        user_msg.append(msg)
            except discord.Forbidden:
                # Bot does not have access to this channel
                pass

        # There are not messages to choose from
        if not user_msg:
            msg = "Unable to take {} out of context!".format(user.name)
            await ctx.send(msg)
            return

        # Get a random message
        rand_msg = random.choice(user_msg)

        # Send embed
        colour = int(self.bot.config.get("colours", "main"), base=16)
        embed = discord.Embed(
            description=rand_msg.clean_content,
            colour=colour,
        )
        embed.set_author(name=user.name, icon_url=user.avatar_url)

        await ctx.send(embed=embed)


def main():
    """ Main function """
    logging.basicConfig(level=logging.INFO)
    config = ConfigParser()
    config.read("settings.ini")

    token = config.get("setup", "token")
    reddit_client = praw.Reddit(client_id=config.get("reddit", "id"),
                                client_secret=config.get("reddit", "secret"),
                                user_agent=config.get("reddit", "agent"))

    bot = Peepo(config)
    bot.add_cog(General(bot, reddit_client))
    bot.run(token)


if __name__ == "__main__":
    main()
